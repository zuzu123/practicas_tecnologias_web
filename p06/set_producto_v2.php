<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: #34E0F7  ;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
		</style>
	</head>
<body>
<?php
$error = array();
/** SE CREA EL OBJETO DE CONEXION */
@$link = new mysqli('localhost', 'root', 'c1h2e3n4a5', 'marketzone');	

/** comprobar la conexión */
if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
}

    if(isset($_POST['name']))
    {
    $nombre = trim($_POST['name']);
    $marca  = trim($_POST['marca']);
    $modelo =  trim($_POST['modelo']);
    $precio =  trim($_POST['precio']);
    $detalles =  trim($_POST['story']);
    $unidades =  trim($_POST['uni']);
    $imagen   =  trim($_POST['img']);
    $eliminado = 0; 
    
    if(empty($nombre))
    {
        $error[]="Ingrese el nombre del produto";
        $code=1;
    }
    if(empty($marca))
    {
        $error[]="Ingrese la marca del produto";
        $code=2;
    }
    if(empty($modelo))
    {
        $error[]="Ingrese el modelo del produto";
        $code=3;
    }
    if(empty($precio))
    {
        $error[]="Ingrese el precio";
        $code=4;
    }
    if(!is_numeric ($precio))
    {
        $error[]="Se aceptan solo números o flotantes";
        $code=4;
    }
    if(empty($detalles))
    {
        $error[]="Ingrese detalles del producto";
        $code=5;
    }
    if(empty($unidades))
    {
        $error[]="Ingrese las unidades del produto";
        $code=6;
    }
    if(!is_numeric($unidades))
    {
        $error[]="Ingresar un número valido";
        $code=6;
    }
    if(empty($imagen))
    {
        $error[]="Insertar una imagen";
        $code=7;
    }
    else
    {
        /** Crear una tabla que no devuelve un conjunto de resultados */
        $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, 'img/{$imagen}', '{$eliminado}')";
    if ( $link->query($sql) ) 
    {
        echo '<fieldset>';
        echo '<h2>Producto insertado con ID:'.$link->insert_id,'</h2>';
        echo '<h2>Propiedades del producto:</h2>';
            echo '<ul>';
                echo '<li><strong>Nombre:</strong> <em>',$nombre,'</em></li>';
                echo '<li><strong>Marca:</strong> <em>',$marca,'</em></li>';
                echo '<li><strong>Modelo:</strong> <em>',$modelo,'</em></li>';
                echo '<li><strong>Precio:</strong> <em>',$precio,'</em></li>';
                echo '<li><strong>Detalles:</strong> <em>',$detalles,'</em></li>';
                echo '<li><strong>Unidades:</strong> <em>',$unidades,'</em></li>';
                echo '<li><strong>Imagen:</strong> <em>',$imagen,'</em></li>';			
            echo '</ul>';
            echo '</fieldset>';
        
    }
    else
    {
        echo 'El Producto no pudo ser insertado =(';
        
    }
    
    $link->close();
    }
    if( count($error) > 0 )
        {
            echo "<p>ERRORES ENCONTRADOS:</p>";
            // Mostrar los errores:
            for( $contador=0; $contador < count($error); $contador++ )
                echo 'Error: ',$contador,'--', $error[$contador]."<br/>";
        }
    
    }

?>
    
</body>
</html>