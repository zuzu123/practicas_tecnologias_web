<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Productos</title>
</head>
<body>
<h1>Registro de productos</h1>

<form id="formularioTenis" action="http://localhost/tecnologiasweb/practicas/p06/set_producto_v2.php" method="post">

    <h2>Inserción de productos</h2>

    <fieldset>
        <legend>Ingrese los datos de los productos</legend>

        <ul>
            <li><label for="form-name">Nombre:</label> <input type="text" name="name" id="form-name"></li>
            <li><label for="form-marca">Marca:</label> <input type="text" name="marca" id="form-marca"></li>
            <li><label for="form-modelo">Modelo:</label> <input type="text" name="modelo" id="form-modelo"></li>
            <li><label for="form-precio">Precio:</label><br><input type="number" name="precio" id="form-precio" step="any"></li>
            <li><label for="form-deta">Detalles:</label><br><textarea name="story" rows="4" cols="60" id="form-story" placeholder="No más de 300 caracteres de longitud"></textarea></li>
            <li><label for="form-uni">Unidades:</label><br><input type="number" name="uni" id="form-uni"></li>
            <li><label for="form-img">Imagen</label><br><input type="file" name="img" id="form-img"></li>
        </ul>
    </fieldset>

    <p>
        <input type="submit" name="enviar" value="Enviar">
        <input type="reset">
    </p>

</form>
    
</body>
</html>