// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarID(e) {
    /**
     * Revisar la siguiente información para entender porqué usar event.preventDefault();
     * http://qbit.com.mx/blog/2013/01/07/la-diferencia-entre-return-false-preventdefault-y-stoppropagation-en-jquery/#:~:text=PreventDefault()%20se%20utiliza%20para,escuche%20a%20trav%C3%A9s%20del%20DOM
     * https://www.geeksforgeeks.org/when-to-use-preventdefault-vs-return-false-in-javascript/
     */
    e.preventDefault();

    // SE OBTIENE EL ID A BUSCAR
    var id = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function() {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n' + client.responseText);

            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText); // similar a eval('('+client.responseText+')');

            // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
            if (Object.keys(productos).length > 0) {
                let a = Object.keys(productos).length;
                let descripcion = "";
                let template = "";

                for (var i = 0; i < a; i++) {
                    descripcion = "";
                    template = "";

                    descripcion += '<li>precio: ' + productos[i].precio + '</li>';
                    descripcion += '<li>unidades: ' + productos[i].unidades + '</li>';
                    descripcion += '<li>modelo: ' + productos[i].modelo + '</li>';
                    descripcion += '<li>marca: ' + productos[i].marca + '</li>';
                    descripcion += '<li>detalles: ' + productos[i].detalles + '</li>';

                    template += `
                        <tr>
                            <td>${productos[i].id}</td>
                            <td>${productos[i].nombre}</td>
                            <td><ul>${descripcion}</ul></td>
                        </tr>
                    `;

                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    document.getElementById("productos").innerHTML += template;
                }
            }
        }
    };
    client.send("id=" + id);
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);
    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;

    //Validaciones
    var correcto = true;
    //expresion para validar alfanumericos
    var expreg = /^([a-zA-Z0-9]){1,25}$/; //admite valores de A a Z, numeros del 0 al 9 y entre 1 a 25 caracteres
    //validación nombre
    var nombre = finalJSON['nombre'];
    if (nombre.length == '') {
        correcto = false;
        alert('El campo nombre esta vacio');
    }
    if (nombre > 100) {
        correcto = false;
        alert('Numero maximo de caracteres son: ');

    }
    //validación editorial
    var edi = finalJSON['marca'];
    if (edi.length == "" || edi.value == 0) {
        correcto = false;
        alert('Eligir editorial');
    }
    //validación unidades
    var uni = finalJSON['unidades'];
    if (uni.length == '') {
        correcto = false;
        alert('Ingrese un número de unidades');
    }
    //validacion modelo
    var modelo = finalJSON['modelo'];
    if (modelo.length == '') {
        correcto = false;
        alert('El campo modelo esta vacio');
    }
    //validacion precio
    var precio = finalJSON['precio'];
    if (precio.length == '') {
        correcto = false;
        alert('Ingrese un precio');
    }

    if (precio <= 99.99) {
        correcto = false;
        alert('Ingrese un precio mayor a 99.99');
    }
    //validacion alfanumerico
    var m = finalJSON['modelo']
    if (!expreg.test(m)) { //validacion con alfanumerico
        correcto = false;
        alert('Solo se admiten valores alfanumericos en el opción MODELO');
    }
    //validación detalles
    var detalles = finalJSON['detalles'];
    if (detalles > 250) {
        correcto = false;
        alert('Número maximo de caracteres son 250');
    }
    if (uni <= 0) {
        correcto = false;
        alert('Ingrese unidades de productos mayor o igual a 0');
    }
    if (correcto) {
        productoJsonString = JSON.stringify(finalJSON, null, 2);

        // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
        var client = getXMLHttpRequest();
        client.open('POST', './backend/create.php', true);
        client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
        client.onreadystatechange = function() {
            // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
            if (client.readyState == 4 && client.status == 200) {
                console.log(client.responseText);
            }
        };
        client.send(productoJsonString);
        window.alert("Se inserto libro")
    }
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try {
        objetoAjax = new XMLHttpRequest();
    } catch (err1) {
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try {
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (err2) {
            try {
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (err3) {
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON, null, 2);
    document.getElementById("description").value = JsonString;
}