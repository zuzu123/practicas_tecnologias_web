<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Productos</title>
</head>
<body>
<h1>Registro de productos</h1>

<form id="formularioTenis"  method="post">

    <fieldset>
        <legend>Actualiza los datos de los productos</legend>

        <ul>
            <li><label for="form-name">Nombre:</label> <input type="text" name="name" id="form-name" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>"></li>
            <li><label for="form-marca">Marca:</label> <input type="text" name="marca" id="form-marca" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>"></li>
            <li><label for="form-modelo">Modelo:</label> <input type="text" name="modelo" id="form-modelo" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>"></li>
            <li><label for="form-precio">Precio:</label><br><input type="number" name="precio" id="form-precio" step="any" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>"></li>
            <li><label for="form-deta">Detalles:</label><br><textarea name="story" rows="4" cols="60" id="form-story" placeholder="No más de 300 caracteres de longitud" ><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea></li>
            <li><label for="form-uni">Unidades:</label><br><input type="number" name="uni" id="form-uni" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>"></li>
            <li><label for="form-img">Imagen</label><br><input type="image" name="imagen" id="form-img" value ="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>"></li>
        </ul>
    </fieldset>

    <p>
        <input type="submit" name="enviar" value="Enviar">
        <input type="reset">
    </p>

</form>
    
</body>
</html>