<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

	<?php
		 /** SE CREA EL OBJETO DE CONEXION */
		 @$link = new mysqli('localhost', 'root', 'c1h2e3n4a5', 'marketzone');
		 /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
 
		 /** comprobar la conexión */
		 if ($link->connect_errno) 
		 {
			 die('Falló la conexión: '.$link->connect_error.'<br/>');
			 //exit();
		 }
 
		 /** Crear una tabla que no devuelve un conjunto de resultados */
		 if ( $result = $link->query("SELECT * FROM productos WHERE eliminado='0'") ) 
		 {
			 /** Se extraen las tuplas obtenidas de la consulta */
			 $row = $result->fetch_all(MYSQLI_ASSOC);
			  
			 /** Se crea un arreglo con la estructura deseada */
			
 
			 /** útil para liberar memoria asociada a un resultado con demasiada información */
			 $result->free();
		 }
 
		 $link->close();
 
		 /** Se devuelven los datos en formato JSON */
	 
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta charset="utf-8">
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<script>
        function show() {
            // se obtiene el id de la fila donde está el botón presinado
            var rowId = event.target.parentNode.parentNode.id;

            // se obtienen los datos de la fila en forma de arreglo
            var data = document.getElementById(rowId).querySelectorAll(".row-data");
            /**
            querySelectorAll() devuelve una lista de elementos (NodeList) que 
            coinciden con el grupo de selectores CSS indicados.
            (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

            En este caso se obtienen todos los datos de la fila con el id encontrado
            y que pertenecen a la clase "row-data".
            */

			var id = data[0].innerHTML;
            var nombre = data[1].innerHTML;
            var marca = data[2].innerHTML;
			var modelo = data[3].innerHTML;
			var precio = data[4].innerHTML;
			var unidades = data[5].innerHTML;
			var detalles = data[6].innerHTML;
			var imagen = data[7].firstChild.getAttribute('src');

            alert("Nombre: " + nombre + "\nMarca: " + marca + "\nModelo" + modelo + "\nPrecio" + precio + "\nUnidades" 
			+ unidades + "\nDetalles" + detalles + "\nImagen" + imagen);

            send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagen);
        }
    </script>
	<body>
		<h3>PRODUCTO</h3>

		<br/>
		<table class="table">
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
					<th scope="col">Submit</th>
					</tr>
				</thead>
		
		<?php foreach($row as $num => $registro) { 
			 ?>
				<tbody>
				<tr id = <?php echo $row[$num]['id'] ?>>
						<th class="row-data"><?php echo $row[$num]['id'] ?></th>
						<td class="row-data"><?php echo $row[$num]['nombre'] ?></td>
						<td class="row-data"><?php echo $row[$num]['marca'] ?></td>
						<td class="row-data"><?php echo $row[$num]['modelo'] ?></td>
						<td class="row-data"><?php echo $row[$num]['precio'] ?></td>
						<td class="row-data"><?php echo $row[$num]['unidades'] ?></td>
						<td class="row-data"><?php echo ($row[$num]['detalles']) ?></td>
						<td class="row-data"><img src=<?php echo $row[$num]['imagen'] ?> ></td>
						<td ><input type="button" value="submit" onclick="show()" /></td>
					</tr>

		<?php } 
		?>
		</tbody>
			</table>

		
	</body>
	<script>
         function send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagen) { //form) { 
            var urlForm = "http://localhost/tecnologiasweb/practicas/p07/formulario_productos_v2.php";
			var propid = "id=" + id;
            var propnombre = "nombre=" + nombre;
            var propmarca = "marca=" + marca;
			var propmodelo = "modelo=" + modelo;
			var propprecio = "precio=" + precio;
			var propunidades = "unidades=" + unidades;
			var propdetalles = "detalles=" + detalles;
			var propimg = "imagen=" + imagen;
            window.open(urlForm + "?" + propid + "&" + propnombre + "&" + propmarca + "&" + propmodelo + "&" +
			propprecio + "&" + propunidades + "&" + propdetalles + "&" + propimg);
        }
    </script>
</html>