<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 3 corregida</title>
</head>
<body>
<?php

echo "<h5>Ejercicio 1</h5>";
//Ejercicio 1
$_myvar = "la";          //valido, comienza con guion bajo.
$_7var = "aaa";          //valido, comienza con guion bajo.
//myvar = "que bueno";   //invalido, le falta el signo $
$myvar = 'ya salio';     //valido, comienza con letra
$var7 = 'no, no sale';   //valido, comienza con letra
$_element1 = 'que bueno';//valido, comienza con guion bajo.
//$house*5 = 'esto no sirve'; //invalido, se usa asterisco 

//echo "$_myvar, $_7var, $myvar,  $var7,  $_element1";


//Ejercicio 2 
echo "<h5>Ejercicio 2: </h5>";

$a = "ManejadorSQL";
$b = 'MySQL';
$c = &$a;
echo "<p>";
echo $a,"<br />";
echo $b,"<br />";
echo $c,"<br />";
echo "c es una referencia para a, por eso imprime el mismo texto";
echo "</p>";
$a = "PHP server";
$b = &$a;
echo "<p>";
echo $a,"<br />";
echo $b,"<br />";
echo $c,"<br />";
echo "<br/>";
echo "El valor de a se actualiza donde se asigna uno nuevo, b ahora va a referenciar a, y c sigue teniendo el mismo valor, que es referenciar a a";
echo "</p>";
echo "<hr />";
//Ejercicio 3 
echo "<h5>Ejercicio 3: </h5>";

$a = "PHP5";
    echo "<p>";
    echo "a: ",$a," ";
	echo "<br />"; 
    $z[] = &$a;
    echo "z: ",$z[0]," ";
	echo "<br />";
    $b = "5a version de PHP";
    echo "b: ",$b," ";
	echo "<br />";
    $c = $b*10;
    echo "c: ",$c," ";
	echo "<br />";
    $a .= $b;
    echo "a: ",$a,"";
	echo "<br />";
    $b *= $c;
    echo "b: ",$b,"";
	echo "<br />";
    $z[0] = "MySQL";
    echo "z: ",$z[0]," ";
    echo "<br />";
    
    echo "</p>";
	echo "<hr />";
//Ejercicio 4
echo "<h5>Ejercicio 4: </h5>";
echo "<p>";
echo $GLOBALS['a'],"<br />";
echo $GLOBALS['b'],"<br />";
echo $GLOBALS['c'],"<br />";
echo $GLOBALS['z[0]'],"<br />";
echo "</p>";
echo "<hr />";
//Ejercicio 5
echo "<h5>Ejercicio 5: </h5>";
$a = "7 personas";
$b = (integer) $a;
$a = "9E3";
$c = (double) $a;
echo "<p>";
echo "valor: ", $a,"<br />";
echo "valor: ", $b,"<br />";
echo "valor: ", $c,"<br />";
echo "</p>";
echo "<hr />";
//Ejercicio 6
echo "<h5>Ejercicio 6: </h5>";
$a = "0";
$b = "TRUE";
$c = FALSE;
$d = ($a OR $b);
$e = ($a AND $c);
$f = ($a XOR $b);
echo "<p>";
echo "'a' es un: ", var_dump($a),"<br />";
echo var_dump($b),"<br />";
echo var_dump($c),"<br />";
echo var_dump($d),"<br />";
echo var_dump($e),"<br />";
echo var_dump($f),"<br />";

//echo 'c es :'.(boolval($c) ? 'true' : 'false')."<br />";
//echo 'e es :'.(boolval($e) ? 'true' : 'false')."\n";
echo "</p>";
echo "<hr />";
//Ejercicio 7
echo "<h5>Ejercicio 7: </h5>";

echo "<p>";
echo "Nombre del servidor: ",$_SERVER['SERVER_NAME'],"<br />"; //nombre del servidor
echo "La version de apache y php: ", $_SERVER['SERVER_SOFTWARE'],"<br />"; //la version de apache y php 
echo "El idioma del navegador: ", $_SERVER['HTTP_ACCEPT_LANGUAGE'],"<br />";// el idioma del navegador
echo "El nombre del sistema operativo: ",$_SERVER['HTTP_USER_AGENT'],"<br />";//el nombre del sistema operativo 
echo "</p>";
echo "<hr />";
echo "<hr />";


?>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
      src="http://www.w3.org/Icons/valid-xhtml11" alt="XHTML 1.1 válido" height="31" width="88" /></a>
  </p>
</body>
</html>


