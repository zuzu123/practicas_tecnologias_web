<?php
    include_once __DIR__.'/database.php';

    $producto = file_get_contents('php://input');
    
    $data = array(
        'status'  => 'error',
        'message' => 'Ya existe un producto con ese nombre'
    );
        if (!empty($producto)) {
            // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);
            $sql = "UPDATE productos SET nombre='{$jsonOBJ->nombre}', 
            marca='{$jsonOBJ->marca}', modelo='{$jsonOBJ->modelo}', precio={$jsonOBJ->precio}, detalles='{$jsonOBJ->detalles}', 
            unidades={$jsonOBJ->unidades}, imagen='{$jsonOBJ->imagen}' WHERE id = {$jsonOBJ->id}";


            if($conexion->query($sql)){
                $data['status'] =  "success";
                $data['message'] =  "Producto actualizado";
            } else {
                $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($conexion);
            }
    $conexion->close();
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
}
?>

