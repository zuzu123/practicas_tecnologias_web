// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON, null, 2);
    document.getElementById("description").value = JsonString;

    // SE LISTAN TODOS LOS PRODUCTOS

}
//Buscar productos
$(document).ready(function() {
    listarProductos();
    let edit = false;
    $('#search').keyup(function(e) {
        if ($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                type: 'POST',
                data: { search },
                success: function(response) {
                    let productos = JSON.parse(response);
                    console.log(productos);
                    let template = '';
                    let template_bar = '';
                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';
                        template += `
                        <tr productId="${producto.id}">
                            <td>${producto.id}</td>
                            <td>${producto.nombre}</td>
                            <td><ul>${descripcion}</ul></td>
                            <td>
                                <button class="product-delete btn btn-danger">
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    `;
                    })
                    productos.forEach(producto => {
                        template_bar += `<li>${producto.nombre}</il>`;

                    })

                    $('#container').html(template_bar);
                    $('#products-result').html(template);
                    $('#products').html(template);
                    // SE HACE VISIBLE LA BARRA DE ESTADO
                    document.getElementById("product-result").className = "card my-4 d-block";
                }

            })

        }
    })




    //Añadir productos
    $('#product-form').submit(function(e) {
        var finalJ = JSON.parse($("#description").val())
        finalJ['id'] = $('#productId').val()
        finalJ['nombre'] = $('#name').val();
        var postData = productoJsonString = JSON.stringify(finalJ, null, 2);
        // console.log(finalJSON);
        e.preventDefault();
        //Validaciones
        var correcto = true;
        var finalJSON = finalJ;
        //expresion para validar alfanumericos
        var expreg = /^([a-zA-Z0-9]){1,25}$/; //admite valores de A a Z, numeros del 0 al 9 y entre 1 a 25 caracteres
        //validación nombre
        var nombre = finalJSON['nombre'];
        if (nombre.length == '') {
            correcto = false;
            alert('El campo nombre esta vacio');
        }
        if (nombre > 100) {
            correcto = false;
            alert('Numero maximo de caracteres son: ');

        }
        //validación editorial
        var edi = finalJSON['marca'];
        if (edi.length == "" || edi.value == 0) {
            correcto = false;
            alert('Eligir editorial');
        }
        //validación unidades
        var uni = finalJSON['unidades'];
        if (uni.length == '') {
            correcto = false;
            alert('Ingrese un número de unidades');
        }
        //validacion modelo
        var modelo = finalJSON['modelo'];
        if (modelo.length == '') {
            correcto = false;
            alert('El campo modelo esta vacio');
        }
        //validacion precio
        var precio = finalJSON['precio'];
        if (precio.length == '') {
            correcto = false;
            alert('Ingrese un precio');
        }

        if (precio <= 99.99) {
            correcto = false;
            alert('Ingrese un precio mayor a 99.99');
        }
        //validacion alfanumerico
        var m = finalJSON['modelo']
        if (!expreg.test(m)) { //validacion con alfanumerico
            correcto = false;
            alert('Solo se admiten valores alfanumericos en el opción MODELO');
        }
        //validación detalles
        var detalles = finalJSON['detalles'];
        if (detalles > 250) {
            correcto = false;
            alert('Número maximo de caracteres son 250');
        }
        if (uni <= 0) {
            correcto = false;
            alert('Ingrese unidades de productos mayor o igual a 0');
        }
        //validacion img
        var img = finalJSON['img'];
        if (img === '') {
            finalJSON = "img/default.png";
        }
        if (correcto) {
            let url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
            console.log(url);
            $.post(url, postData, function(response) {
                console.log(response);
                let respuesta = JSON.parse(response);
                //listarProductos();
                let template_bar = '';
                template_bar = `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                //$('#products').trigger('reset');
                $('#product-result').html(template_bar);
                document.getElementById("product-result").className = "card my-4 d-block";
                listarProductos();
            });
        } else {
            let operacion = 'inserTAR';
            if (edit) {
                operacion = 'ediTAR'
            }
            let template = '';
            template = `
                            <li style="list-style: none;">status: No se ingreso producto en la operacion ${operacion}</li>
                        `;
            //$('#products').trigger('reset');
            $('#product-result').html(template);
            document.getElementById("product-result").className = "card my-4 d-block";
        }
        var JsonString = JSON.stringify(baseJSON, null, 2);
        listarProductos()
        $('#product-form').trigger('reset');
        document.getElementById("description").value = JsonString;
    });


    //eliminar productos
    $(document).on('click', '.product-delete', function() {

        if (confirm('Estas seguro de eliminar el producto?')) {
            let elemento = $(this)[0].parentElement.parentElement;
            let id = $(elemento).attr('productId');
            $.post('backend/product-delete.php', { id }, function(response) {
                //console.log(response);
                let respuesta = JSON.parse(response);
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                $('#product-result').html(template_bar);
                $('#container').html(template_bar);
                // SE HACE VISIBLE LA BARRA DE ESTADO
                document.getElementById("product-result").className = "card my-4 d-block";
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                //document.getElementById("container").innerHTML = template_bar;
                listarProductos();
            })
        }
    })

    //editar
    $(document).on('click', '.product-item', function() {
        let elemento = $(this)[0].parentElement.parentElement;
        let id = $(elemento).attr('productId');
        $.post('backend/product-unic.php', { id }, function(response) {
            console.log(response);
            let producto = JSON.parse(response);
            $('#name').val(producto.nombre);
            let descripcion = `{
                "precio": ${ producto.precio },
                "unidades": ${producto.unidades},
                "modelo": "${producto.modelo}",
                "marca": "${producto.marca}",
                "detalles": "${producto.detalles}",
                "imagen": "${producto.imagen}"
                }`;
            $('#description').val(descripcion);
            $('#productId').val(id);
            edit = true;

            //$('#description').html(template);
        })
    })


    //Lista de productos
    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                let productos = JSON.parse(response);
                let template = '';

                productos.forEach(producto => {
                    // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                    let descripcion = '';
                    descripcion += '<li>precio: ' + producto.precio + '</li>';
                    descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                    descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                    descripcion += '<li>marca: ' + producto.marca + '</li>';
                    descripcion += '<li>detalles: ' + producto.detalles + '</li>';
                    template += `
                                             <tr productId="${producto.id}">
                                             <td>${producto.id}</td>
                                             <td>
                                             <a href="#" class="product-item">${producto.nombre}</a>
                                             </td>
                                            <td><ul>${descripcion}</ul></td>
                                              <td>
                                            <button class="product-delete btn btn-danger" >
                                            Eliminar
                                            </button>
                                            </td>
                                              </tr> `;
                });
                $('#products').html(template);
            }
        })
    }
});